<?php
/**
 * Created by PhpStorm.
 * User: pyvil
 * Date: 05.07.16
 * Time: 22:25
 */

namespace core;
use core\db\Model;

/**
 * Class Session
 *
 * @package core
 */
class Session
{
    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public static function setData($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * @param $key
     * @return null
     */
    public static function getData($key)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }
        return null;
    }

    /**
     * @return Model
     */
    public static function getUser()
    {
        return static::getData('user');
    }

    /**
     * @return array
     */
    public static function getUserId()
    {
        return static::getUser()->getData(static::getUser()->primaryKey);
    }

    /**
     * @param $user
     * @param $data
     *
     * @return bool
     * @throws \Exception
     */
    public static function login($user, $data)
    {
        if (!isset($data['username']) || !isset($data['password'])) {
           return false;
        }
        /** @var Model $user */
        $user = $user->where(array('username' => $data['username']))
            ->andWhere(array('password' => App::passwordHash($data['password'])))
            ->load();
        static::setData('user', $user);
        return $user;
    }

    /**
     * @return bool
     */
    public static function getIsLoggedIn()
    {
        return static::getUser() && static::getUser()->getData('auth_key');
    }
}