<?php
return array(
    'db' => array(
        'host' => '127.0.0.1',
        'db_name' => 'task',
        'username' => 'root',
        'password' => 'root'
    ),
    'router' => require_once 'router.php'
);