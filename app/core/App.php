<?php

namespace core;
use core\db\orm\Orm;
use core\http\Request;

/**
 * Class App
 *
 * @package core
 */
final class App
{
    /**
     * @var ORM|null
     */
    protected $_orm = null;
    /**
     * @var Request|null
     */
    protected static $_request = null;
    /**
     * @var mixed|null
     */
    protected static $_config = null;
    /**
     * @var Session
     */
    protected static $_session = null;

    /**
     * App constructor.
     */
    public function __construct()
    {
        if (!static::$_config) {
            static::setConfig(require_once __DIR__ . '/../config/main.php');
        }
        if (!static::$_session || !$_SESSION || !isset($_SESSION)) {
            static::$_session = new Session();
        }
        if (!static::$_request) {
            static::$_request = new Request();
        }
        $this->_orm = new ORM();
    }

    /**
     * Set config
     *
     * @param array $config config
     *
     * @return void
     */
    public static function setConfig(array $config)
    {
        static::$_config = $config;
    }

    /**
     * Get config
     *
     * @param null $key
     *
     * @return mixed|null
     */
    public static function getConfig($key = null) {
        if (isset(static::$_config[$key])){
            return static::$_config[$key];
        }
        return static::$_config;
    }

    /**
     * @return Session
     */
    public static function getSession()
    {
        return static::$_session;
    }

    /**
     * Orm
     *
     * @return Orm|null
     */
    public function getOrm()
    {
        return $this->_orm;
    }

    /**
     * Request
     *
     * @return Request|null
     */
    public static function getRequest()
    {
        return static::$_request;
    }

    /**
     * @param int $length
     *
     * @return string
     */
    public static function generateRandomString($length = 32) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @param $password
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public static function passwordHash($password)
    {
        if (function_exists('password_hash')) {
            /** @noinspection PhpUndefinedConstantInspection */
            return password_hash($password, PASSWORD_DEFAULT);
        }

        $hash = crypt($password);
        // strlen() is safe since crypt() returns only ascii
        if (!is_string($hash) || strlen($hash) !== 60) {
            throw new \Exception('Unknown error occurred while generating hash.');
        }

        return $hash;
    }


    /**
     * Run env
     *
     * @return void
     */
    public function run()
    {
        $data = static::$_request->handle();
        if (!$data) {
            return;
        }
        $data['controller'] = 'local\\controller\\' . $data['controller'];
        $controller = new $data['controller']();
        $data = $controller->{$data['action']}($data['params']);
        if (!empty($data)) {
            echo $data;
        }
    }
}